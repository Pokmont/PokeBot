#!/usr/bin/python3
# -*- coding: utf-8 -*-

import logging
import discord
from datetime import datetime, timedelta
from random import randint
from .DiscordAlarm import Alarm
from .utils import (get_args, Dicts, reject_leftover_parameters, get_color,
                    get_static_map_url, get_image_url)
from .commands import pause

log = logging.getLogger('Notification')
args = get_args()


class Notification(Alarm):

    _defaults = {
        'pokemon': {
            'content': "",
            'icon_url': get_image_url(
                "monsters/<pkmn_id_3>_<form_id_or_empty>.png"),
            'title': "A wild <pkmn> has appeared!",
            'url': "<gmaps>",
            'body': "Available until <24h_time> (<time_left>).",
            'color': "<iv>"
        },
        'egg': {
            'content': "",
            'icon_url': get_image_url("eggs/<raid_level>.png"),
            'title': "Raid is incoming!",
            'url': "<gmaps>",
            'body': (
                "A level <raid_level> raid will hatch <begin_24h_time> " +
                "(<begin_time_left>)."
            ),
            'color': "<raid_level>"
        },
        'raid': {
            'content': "",
            'icon_url': get_image_url(
                "monsters/<pkmn_id_3>_<form_id_or_empty>.png"),
            'title': "Level <raid_level> Raid is available against <pkmn>!",
            'url': "<gmaps>",
            'body': "The raid is available until <24h_time> (<time_left>).",
            'color': "<raid_level>"
        }
    }

    def __init__(self, settings):
        self.__map = settings.pop('map', {})
        self.__pokemon = self.create_alert_settings(
            settings.pop('pokemon', {}), self._defaults['pokemon'])
        self.__egg = self.create_alert_settings(
            settings.pop('egg', {}), self._defaults['egg'])
        self.__raid = self.create_alert_settings(
            settings.pop('raid', {}), self._defaults['raid'])
        reject_leftover_parameters(settings, "'Alarm level in DM alarm.")
        log.info("DM Alarm has been created!")

    def create_alert_settings(self, settings, default):
        alert = {
            'content': settings.pop('content', default['content']),
            'icon_url': settings.pop('icon_url', default['icon_url']),
            'title': settings.pop('title', default['title']),
            'url': settings.pop('url', default['url']),
            'body': settings.pop('body', default['body']),
            'color': default['color'],
            'map': get_static_map_url(
                settings.pop('map', self.__map), args.gmaps_keys[randint(
                    0, len(args.gmaps_keys) - 1)])
        }
        reject_leftover_parameters(settings, "'Alert level in DM alarm.")
        return alert

    async def send_alert(self, client, bot_number, alert, info, user_ids):
        msg = self.replace(alert['content'], info)
        em = discord.Embed(
            title=self.replace(alert['title'], info),
            url=self.replace(alert['url'], info),
            description=self.replace(alert['body'], info),
            color=get_color(self.replace(alert['color'], info))
        )
        em.set_thumbnail(url=self.replace(alert['icon_url'], info))
        if alert['map'] is not None:
            em.set_image(
                url=self.replace(alert['map'], {
                    'lat': info['lat'],
                    'lng': info['lng']
                })
            )

        rate_limit_dm = discord.Embed(
            title='DM Alert Rate Limit Exceeded',
            description=(
                'The rate at which you receive DM Alerts has ' +
                '**exceeded the maximum** allowed number of DMs: (' +
                '{} DMs in a {} minute period). '.format(
                    args.max_dms_period,
                    args.dm_rate_limit_period) +
                'Please adjust your filters ' +
                'to ensure you stay within the allowable ' +
                'limit. **DM alerts have been paused.** ' +
                'Once you have finished adjusting your ' +
                'filters, execute the !r command to resume ' +
                'receiving DMs.  Thank you.'),
            color=0xff0000
        )

        user_dms = Dicts.bots[bot_number]['user_dms']
        log.info("User IDs: {}".format(user_ids))
        for user_id in user_ids:
            if user_id not in user_dms:
                user_dms[user_id] = {
                    'period_start': datetime.utcnow(),
                    'count': 0,
                    'warning_sent': False
                }

            if (datetime.utcnow() - user_dms[user_id]['period_start'] >
                    timedelta(minutes=args.dm_rate_limit_period)):
                user_dms[user_id]['period_start'] = datetime.utcnow()
                user_dms[user_id]['count'] = 0
                user_dms[user_id]['warning_sent'] = False
            else:
                user_dms[user_id]['count'] += 1

            if user_dms[user_id]['count'] <= args.max_dms_period:
                dest = client.get_user(user_id)
                if dest is not None:
                    log.info("Sending alert to {}".format(user_id))
                    log.info("Destination: {}".format(dest))
                    await Dicts.bots[bot_number]['out_queue'].put((
                        2, Dicts.bots[bot_number]['count']*1000 +
                        randint(0, 9999), {
                            'destination': dest,
                            'msg': msg,
                            'embed': em,
                            'timestamp': datetime.utcnow()
                        }
                    ))
                    Dicts.bots[bot_number]['count'] += 1
            elif user_dms[user_id]['warning_sent'] is False:
                dest = client.get_user(user_id)
                if dest is not None:
                    await Dicts.bots[bot_number]['out_queue'].put((
                        2, Dicts.bots[bot_number]['count']*1000 +
                        randint(0, 9999), {
                            'destination': dest,
                            'embed': rate_limit_dm,
                            'timestamp': datetime.utcnow()
                        }
                    ))
                    await pause(bot_number, dest, None)
                    log.info('User {} has exceeded {} DMs '.format(
                        dest, args.max_dms_period) +
                        'in a {} minute '.format(args.dm_rate_limit_period) +
                        ' period. Alerts for this user have been paused.')
                    user_dms[user_id]['warning_sent'] = True
                    Dicts.bots[bot_number]['count'] += 1

    async def pokemon_alert(self, client, bot_number, pokemon_info, user_ids):
        await self.send_alert(
            client, bot_number, self.__pokemon, pokemon_info, user_ids)

    async def raid_egg_alert(self, client, bot_number, raid_info, user_ids):
        await self.send_alert(client, bot_number, self.__egg, raid_info,
                              user_ids)

    async def raid_alert(self, client, bot_number, raid_info, user_ids):
        await self.send_alert(client, bot_number, self.__raid, raid_info,
                              user_ids)
